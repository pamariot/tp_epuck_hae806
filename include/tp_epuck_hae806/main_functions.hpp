#pragma once

#include <epuck/robot.hpp>
#include <epuck/logger.hpp>

#include <array>
#include <chrono>
#include <string_view>

constexpr std::string_view color_cout_reset = "\033[0m";

constexpr std::string_view color_cout_black = "\033[0;30m";
constexpr std::string_view color_cout_red = "\033[0;31m";
constexpr std::string_view color_cout_green = "\033[0;32m";
constexpr std::string_view color_cout_yellow = "\033[0;33m";
constexpr std::string_view color_cout_blue = "\033[0;34m";
constexpr std::string_view color_cout_magenta = "\033[0;35m";
constexpr std::string_view color_cout_cyan = "\033[0;36m";
constexpr std::string_view color_cout_white = "\033[0;37m";

constexpr std::string_view color_cout_black_bright = "\033[1;30m";
constexpr std::string_view color_cout_red_bright = "\033[1;31m";
constexpr std::string_view color_cout_green_bright = "\033[1;32m";
constexpr std::string_view color_cout_yellow_bright = "\033[1;33m";
constexpr std::string_view color_cout_blue_bright = "\033[1;34m";
constexpr std::string_view color_cout_magenta_bright = "\033[1;35m";
constexpr std::string_view color_cout_cyan_bright = "\033[1;36m";
constexpr std::string_view color_cout_white_bright = "\033[1;37m";

void parseArguments(Robot& robot, int argc, char** argv);
void incorrectArguments(const int& argc);

void proxDataRawValuesToMeters(Robot& robot);

struct OdometryCorrectionData {
    double left_steps_diff{}, right_steps_diff{};
    double left_steps_prev{}, right_steps_prev{};
    double delta_steps{}, delta_theta{};
    double left_steps_diff_avg{}, right_steps_diff_avg{}, time_ten_iter{};
    Pose pose;
};

void odometryAndWheelsSpeed(Robot& robot, OdometryCorrectionData& data,
                            size_t iteration,
                            std::chrono::duration<double> iteration_delta_time);

void showAndSaveRobotImage(const Robot& robot, Logger& logger, size_t iteration);
void saveProximitySensors(const Robot& robot, Logger& logger);
void savePosition(const Robot& robot, Logger& logger);
void printSensors(const Robot& robot,
                  std::chrono::duration<double> time_since_start);